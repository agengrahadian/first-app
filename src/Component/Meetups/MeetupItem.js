import classes from './MeetupItem.module.css';
import Card from '../Ui/Card';
import {Link} from 'react-router-dom';

const MeetupItem = (props) => {
    // const Profile = () => {
    //     componentDidMount () {
    //         const { handle } = this.props.match.params
    //         const { title } = this.props.location.state
    //     }
    //     render() {
            
    //     }
    // }

    return (
        <li className={classes.item}>
            <Card>
                <div className={classes.image}>
                    <img src={props.image} alt={props.title}/>
                </div>
                <div className={classes.content}>
                    <h3>{props.title}</h3>
                    <p>{props.description}</p>
                </div>
                <div className={classes.actions}>
                    <button>To Favorites</button>
                </div>
                <div>
                    <button>
                        <Link to={{
                            pathname:"/profile",
                            query: props
                            }
                        }>Profile</Link>
                    </button>
                </div>
            </Card>
        </li>
    )
}
export default MeetupItem;