import './App.css';
import Person from './Component/Person';
import {Route, Switch} from 'react-router-dom';
import AllMeetups from './Pages/AllMeetups';
import Favorites from './Pages/Favorites';
import NewMeetups from './Pages/NewMeetups';
import MainNavigation from './Component/Layout/MainNavigation';
import Profile from './Pages/Profile';
import Card2 from './Component/Ui/Card2';

function App() {
  return (
    // <div>  
    //   {/* <div className="App">
    //       Ini aplikasi react saya
    //       <Person name='Ageng Rahadian Adinegoro' pos='Programmer Analyst'/>
    //       <Person name='Hendra Kurniawan' pos='Frontend Developer'/>
    //       <Person name='Yudistira Nur Sasongko' pos='Backend Developer'/>
    //   </div> */}

      <div>
        <MainNavigation/>
          {/* <Card2/> */}
          <Switch>
            <Route path='/' exact>
              <AllMeetups/>
            </Route>
            <Route path='/new-meetups'>
              <NewMeetups/>
            </Route>
            <Route path='/favorites'>
              <Favorites/>
            </Route>
            <Route path='/person-aja'>
              <Person name='Ageng Rahadian Adinegoro' pos='Programmer Analyst'/>
              <Person name='Hendra Kurniawan' pos='Frontend Developer'/>
              <Person name='Yudistira Nur Sasongko' pos='Backend Developer'/>
            </Route>
            <Route path='/profile' component={Profile}/>
          </Switch>
      </div>
    // </div>
  );
}

export default App;
