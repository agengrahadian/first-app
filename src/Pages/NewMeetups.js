import React from "react";
import { useState, useEffect } from "react";
import axios from 'axios';

const NewMeetups = () => {
    const [isi, setIsi] = useState({ email: "", password: "" });
    const [count, setCount] = useState(0);
    const [countPost, setcountPost] = useState(true);
    const [countComment, setcountComment] = useState(false);
    const [values, setValues] = useState();

    // useEffect(() => {
    //     console.log("test");
    // }, []);

    const getPostHandler = (parameter_post) => {
        if (parameter_post) {
            setcountPost(true);
            setcountComment(false);
        } else{
            setcountPost(false);
            setcountComment(true);
        };
    };

    useEffect(() => {
        const getData = async () => {
            const req = await axios.get("https://jsonplaceholder.typicode.com/posts")
            console.log(req.data);
            setValues(req.data);
        }
        if(countPost === true && countComment === false){
            getData();
        }
        
    }, [countPost,countComment]);

    useEffect(() => {
        const getDataComment = async () => {
            const req = await axios.get("https://jsonplaceholder.typicode.com/comments?postId=1")
            console.log(req.data);
            setValues(req.data);
        }
        if(countPost === false && countComment === true){
            getDataComment();
        }
    }, [countPost,countComment]);

    return (
        <>
            <div>
                Email :
                <input
                    name="email"
                    type="email"
                    value={isi.email}
                    onChange={setIsi}
                />
            </div>
            <div>
                Password :
                <input
                    name="password"
                    type="password"
                    value={isi.password}
                    onChange={setIsi}
                />
            </div>
            <div>
                <p>Post</p>
                <button onClick={() => getPostHandler(true)}>
                    Click me
            </button>
            </div>

            <div>
                <p>Comment</p>
                <button onClick={() => getPostHandler(false)}>
                    Click me
            </button>
            </div>

            <div>
                data get result:
                {/* {JSON.stringify(values)} */}
                {/* {values && values.map((xyz,index) => (
                    <div key={index}>
                        <div>{xyz.id}</div>
                        <div>{xyz.title}</div>
                        <div>{xyz.body}</div>
                    </div>
                ))} */}
            </div>
        </>
    )
}
export default NewMeetups;