import MeetupList from '../Component/Meetups/MeetupList';

const DUMMY_DATA = [
    {
        id:'id1',
        title:'Xing Fu Tang',
        image: 'https://sgp1.digitaloceanspaces.com/tz-mag-id/wp-content/uploads/2019/09/020209090505/Boba-1.jpg',
        description: 'minuman boba Xing Fu Tang'
    },
    {
        id:'id2',
        title:'Tiger Sugar',
        image: 'https://sgp1.digitaloceanspaces.com/tz-mag-id/wp-content/uploads/2019/09/020209095656/Boba-2.jpg',
        description: 'minuman boba Tiger Sugar'
    }
];

const AllMeetups = () => {
    return(
        <section>
            <h1>The Bobas</h1>
            <MeetupList meetups={DUMMY_DATA}/>
        </section>
    )
}
export default AllMeetups;